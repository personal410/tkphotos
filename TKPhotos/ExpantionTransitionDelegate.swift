//
//  ExpantionTransitionDelegate.swift
//  TKPhotos
//
//  Created by victor salazar on 21/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class ExpantionTransitionDelegate:NSObject, UIViewControllerAnimatedTransitioning{
    var isPresenting = false
    var firstScale:CGFloat = 0
    func transitionDuration(transitionContext:UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    func animateTransition(transitionContext:UIViewControllerContextTransitioning){
        transitionContext.containerView()!.backgroundColor = UIColor.whiteColor()
        let toViewCont = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let fromViewCont = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        toViewCont.view.frame = fromViewCont.view.frame
        let homeViewCont = (isPresenting ? fromViewCont : toViewCont) as! HomeViewController
        let photoViewCont = (isPresenting ? toViewCont : fromViewCont) as! PhotoViewController
        let previousFrame = photoViewCont.photoImgView.frame
        let selectedCell = homeViewCont.photosCollectionView.cellForItemAtIndexPath(homeViewCont.selectedIndexPath!)!
        var newFrame = selectedCell.frame
        if isPresenting {
            transitionContext.containerView()!.addSubview(photoViewCont.view)
            photoViewCont.photoImgView.frame = selectedCell.frame
            selectedCell.alpha = 0.0
        }else{
            transitionContext.containerView()!.insertSubview(homeViewCont.view, belowSubview: photoViewCont.view)
            if homeViewCont.isLandscape != Toolbox.isLandscape() {
                let layout = homeViewCont.photosCollectionView.collectionViewLayout as! PhotosLayout
                layout.prepareLayout()
                homeViewCont.photosCollectionView.reloadData()
                newFrame = layout.cache[homeViewCont.selectedIndexPath!.row].frame
            }
        }
        UIView.animateWithDuration(self.transitionDuration(transitionContext), animations: {
            if self.isPresenting {
                photoViewCont.photoImgView.frame = previousFrame
            }else{
                photoViewCont.photoImgView.frame = newFrame
            }
        }){(b:Bool) in
            if !self.isPresenting {
                homeViewCont.photosCollectionView.cellForItemAtIndexPath(homeViewCont.selectedIndexPath!)!.alpha = 1.0
                homeViewCont.selectedIndexPath = nil
                homeViewCont.isLandscape = Toolbox.isLandscape()
            }
            fromViewCont.view.removeFromSuperview()
            transitionContext.completeTransition(true)
        }
    }
}