//
//  ViewController.swift
//  TKPhotos
//
//  Created by Victor Salazar on 18/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class HomeViewController:UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, PhotosLayoutDelegate {
    //MARK: - Variables
    var arrPhotos:Array<Photo> = []
    var tempImgView = UIImageView()
    var selectedIndexPath:NSIndexPath?
    var isLandscape = false
    //MARK: - IBOutlet
    @IBOutlet weak var photosCollectionView:UICollectionView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.view.addSubview(self.tempImgView)
        let layout = self.photosCollectionView.collectionViewLayout as! PhotosLayout
        layout.delegate = self
        self.isLandscape = Toolbox.isLandscape()
    }
    override func viewWillAppear(animated:Bool){
        super.viewWillAppear(animated)
        let newArrPhotos = Photo.getAllPhotos()
        if newArrPhotos.count > self.arrPhotos.count {
            self.arrPhotos = Photo.getAllPhotos()
            self.photosCollectionView.reloadData()
        }
        if self.isLandscape != Toolbox.isLandscape() {
            let layout = self.photosCollectionView.collectionViewLayout as! PhotosLayout
            layout.invalidateLayout()
        }
    }
    override func willRotateToInterfaceOrientation(toInterfaceOrientation:UIInterfaceOrientation, duration:NSTimeInterval){
        let layout = self.photosCollectionView.collectionViewLayout as! PhotosLayout
        layout.invalidateLayout()
        self.isLandscape = UIInterfaceOrientationIsLandscape(toInterfaceOrientation)
    }
    override func prepareForSegue(segue:UIStoryboardSegue, sender:AnyObject?){
        if let viewCont = segue.destinationViewController as? PhotoViewController {
            self.selectedIndexPath = self.photosCollectionView.indexPathsForSelectedItems()!.first!
            viewCont.photo = self.arrPhotos[selectedIndexPath!.row]
        }
    }
    //MARK: - CollectionView
    func collectionView(collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        return arrPhotos.count
    }
    func collectionView(collectionView:UICollectionView, cellForItemAtIndexPath indexPath:NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! PhotoCollectionViewCell
        let photo = self.arrPhotos[indexPath.row]
        cell.photoImgView.image = UIImage(data: photo.final_image)
        cell.alpha = 1.0
        if let selectedIndexPathTemp = selectedIndexPath {
            if indexPath == selectedIndexPathTemp {
                cell.alpha = 0.0
            }
        }
        return cell
    }
    func collectionView(collectionView:UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("showPhotoViewCont", sender: nil)
    }
    //MARK: - PhotosLayout
    func collectionView(collectionView:UICollectionView, sizeForPhotoAtIndexPath indexPath:NSIndexPath) -> CGSize {
        let photo = self.arrPhotos[indexPath.row]
        return photo.getSize()
    }
}