//
//  CreateImageViewController.swift
//  TKPhotos
//
//  Created by Victor Salazar on 18/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class CreateImageViewController:UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    //MARK: - Variabes
    var image:UIImage?
    var imageHeight:CGFloat = 0
    var imageWidth:CGFloat = 0
    var photoImgView = UIImageView()
    var tempImgView = UIImageView()
    var didSwiped = false
    var drawingLine = false
    var lastPoint = CGPointZero
    var currentList = 0
    
    var isLandscape:Bool = false
    var arrStrokes:Array<(colorIdx:Int, lineWidth:CGFloat, arrSubstrokes:Array<(starPoint:CGPoint, endPoint:CGPoint)>)> = []
    var arrDetailStrokes:Array<(starPoint:CGPoint, endPoint:CGPoint)> = []
    var actualScale:CGFloat = 0
    var portraitScale:CGFloat = 0
    var landscapeScale:CGFloat = 0
    var factorLineWidth:CGFloat = 0
    var currentLineWidth:CGFloat = 2
    var currentLineColor:Int = 0
    let arrColors = Toolbox.arrAvailableColors()
    let arrLineTypes = ["Delgada", "Mediana", "Gruesa"]
    
    //MARK: - IBOutlet
    @IBOutlet weak var colorsCollectionView:UICollectionView!
    @IBOutlet weak var addImageBtn:UIButton!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.view.addSubview(self.photoImgView)
        self.view.addSubview(self.tempImgView)
    }
    override func viewWillAppear(animated:Bool){
        super.viewWillAppear(animated)
        self.navigationController?.toolbarHidden = false
    }
    override func viewWillDisappear(animated:Bool){
        super.viewWillDisappear(animated)
        self.navigationController?.toolbarHidden = true
    }
    override func willRotateToInterfaceOrientation(toInterfaceOrientation:UIInterfaceOrientation, duration:NSTimeInterval){
        if self.image != nil {
            UIView.animateWithDuration(0.25, animations:{
                let aditional:CGFloat = UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 108 : 64
                let diff:CGFloat = UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 64 : 108
                print(self.view.frame.height)
                print(self.view.frame.width)
                self.photoImgView.frame = self.calculatePhotoFrame(self.view.frame.height + aditional, height: self.view.frame.width - diff)
            }, completion:{(b:Bool) in
                self.tempImgView.frame = self.photoImgView.frame
            })
        }
    }
    //MARK: - IBAction
    @IBAction func savePhoto(){
        if self.image != nil {
            if Photo.createNewPhoto(self.image!, finalImg:self.photoImgView.image!, arrStrokes: self.arrStrokes) {
                self.navigationController?.popToRootViewControllerAnimated(true)
            }else{
                print("no se pudo guardar")
            }
        }
    }
    @IBAction func addPhoto(){
        let alertCont = UIAlertController(title: "Agregar imagen", message: nil, preferredStyle: .ActionSheet)
        alertCont.addAction(UIAlertAction(title: "Desde la libreria", style: .Default, handler:{(action:UIAlertAction) -> Void in
            self.showImagePickerContWithOption(1)
        }))
        alertCont.addAction(UIAlertAction(title: "Tomar foto", style: .Default, handler:{(action:UIAlertAction) -> Void in
            self.showImagePickerContWithOption(0)
        }))
        alertCont.addAction(UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil))
        self.presentViewController(alertCont, animated: true, completion: nil)
    }
    @IBAction func showColorPallete(){
        self.view.bringSubviewToFront(self.colorsCollectionView)
        if self.colorsCollectionView.hidden {
            self.currentList = 0
            self.colorsCollectionView.reloadData()
            self.colorsCollectionView.hidden = false
            self.colorsCollectionView.transform = CGAffineTransformMakeTranslation(0, self.colorsCollectionView.frame.height)
            UIView.animateWithDuration(0.5, animations: {
                self.colorsCollectionView.transform = CGAffineTransformIdentity
            })
        }else{
            if self.currentList == 0 {
                self.hideColorPallete()
            }else{
                self.currentList = 0
                self.colorsCollectionView.reloadData()
            }
        }
    }
    @IBAction func showLineOptions(){
        self.view.bringSubviewToFront(self.colorsCollectionView)
        if self.colorsCollectionView.hidden {
            self.currentList = 1
            self.colorsCollectionView.reloadData()
            self.colorsCollectionView.hidden = false
            self.colorsCollectionView.transform = CGAffineTransformMakeTranslation(0, self.colorsCollectionView.frame.height)
            UIView.animateWithDuration(0.5, animations: {
                self.colorsCollectionView.transform = CGAffineTransformIdentity
            })
        }else{
            if self.currentList == 1 {
                self.hideColorPallete()
            }else{
                self.currentList = 1
                self.colorsCollectionView.reloadData()
            }
        }
    }
    //MARK: - Auxiliar
    func showImagePickerContWithOption(option:Int){
        let sourceType:UIImagePickerControllerSourceType = (option == 0) ? .Camera : .PhotoLibrary
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            let imagePickerViewCont = UIImagePickerController()
            imagePickerViewCont.sourceType = sourceType
            imagePickerViewCont.delegate = self
            imagePickerViewCont.mediaTypes = ["public.image"]
            UIApplication.sharedApplication().statusBarStyle = .Default
            self.presentViewController(imagePickerViewCont, animated: true, completion: nil)
        }else{
            let message = (option == 0) ? "Camara no disponible" : "No se puede acceder a su libreria de fotos"
            Toolbox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
        }
    }
    func hideColorPallete(){
        UIView.animateWithDuration(0.5, animations: {
            self.colorsCollectionView.transform = CGAffineTransformMakeTranslation(0, self.colorsCollectionView.frame.height)
            }, completion:{(b:Bool) in
                self.colorsCollectionView.hidden = true
        })
    }
    //MARK: - ImagePicker
    func imagePickerController(picker:UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        self.dismissViewControllerAnimated(true, completion: {
            if mediaType == "public.image" {
                self.image = info[UIImagePickerControllerOriginalImage] as? UIImage
                self.photoImgView.image = self.image
                self.addImageBtn.hidden = true
                self.navigationItem.rightBarButtonItem?.enabled = true
                var avaibleWidth = self.view.frame.width
                var availableHeight = self.view.frame.height
                if avaibleWidth > availableHeight {
                    let heightTemp = availableHeight
                    availableHeight = avaibleWidth - 108
                    avaibleWidth = heightTemp + 64
                }
                let avaiblePortraitHeight = availableHeight
                let scaleWidthPor = self.image!.size.width / avaibleWidth
                let scaleHeightPor = self.image!.size.height / avaiblePortraitHeight
                self.portraitScale = max(scaleWidthPor, scaleHeightPor)
                let avaibleLandscapeHeight = avaibleWidth - 44
                let scaleWidthLand = self.image!.size.width / availableHeight
                let scaleHeightLand = self.image!.size.height / avaibleLandscapeHeight
                self.landscapeScale = max(scaleWidthLand, scaleHeightLand)
                self.factorLineWidth = self.portraitScale / self.landscapeScale
                self.photoImgView.frame = self.calculatePhotoFrame(self.view.frame.width, height: self.view.frame.height)
                self.tempImgView.frame = self.photoImgView.frame
            }
        })
    }
    func imagePickerControllerDidCancel(picker:UIImagePickerController){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //MARK: - Auxiliar
    func calculatePhotoFrame(width:CGFloat, height:CGFloat) -> CGRect {
        var y:CGFloat = 0
        if width > height {
            self.isLandscape = true
            self.actualScale = self.landscapeScale
        }else{
            self.isLandscape = false
            self.actualScale = self.portraitScale
        }
        self.imageWidth = floor(self.image!.size.width / self.actualScale)
        self.imageHeight = floor(self.image!.size.height / self.actualScale)
        y += (height - self.imageHeight) / 2
        let x:CGFloat = (width - self.imageWidth) / 2
        return CGRect(x: x, y: y, width: self.imageWidth, height: self.imageHeight)
    }
    func drawLineFrom(fromPoint:CGPoint, toPoint:CGPoint){
        let newDetailStroke = (fromPoint * self.actualScale, toPoint * self.actualScale)
        self.arrDetailStrokes.append(newDetailStroke)
        UIGraphicsBeginImageContext(self.tempImgView.frame.size)
        let context = UIGraphicsGetCurrentContext()
        self.tempImgView.image?.drawInRect(CGRect(x: 0, y: 0, width: self.tempImgView.frame.size.width, height: self.tempImgView.frame.size.height))
        CGContextMoveToPoint(context, fromPoint.x, fromPoint.y)
        CGContextAddLineToPoint(context, toPoint.x, toPoint.y)
        CGContextSetLineCap(context, CGLineCap.Round)
        let factor = self.isLandscape ? self.factorLineWidth : 1
        CGContextSetLineWidth(context, self.currentLineWidth * factor)
        CGContextSetStrokeColorWithColor(context, self.arrColors[currentLineColor].CGColor)
        CGContextStrokePath(context)
        self.tempImgView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    //MARK: - Touch
    override func touchesBegan(touches:Set<UITouch>, withEvent event:UIEvent?){
        if touches.count == 1 {
            let firstTouch = touches.first!
            let point = firstTouch.locationInView(self.view)
            if CGRectContainsPoint(self.photoImgView.frame, point){
                if !self.colorsCollectionView.hidden {
                    self.hideColorPallete()
                }
                self.drawingLine = true
                self.lastPoint = firstTouch.locationInView(self.photoImgView)
                self.didSwiped = false
            }
        }
    }
    override func touchesMoved(touches:Set<UITouch>, withEvent event:UIEvent?){
        if self.drawingLine {
            self.didSwiped = true
            let firstTouch = touches.first!
            let currentPoint = firstTouch.locationInView(self.photoImgView)
            self.drawLineFrom(self.lastPoint, toPoint: currentPoint)
            self.lastPoint = currentPoint
        }
    }
    override func touchesEnded(touches:Set<UITouch>, withEvent event:UIEvent?){
        if self.drawingLine {
            if !self.didSwiped {
                self.drawLineFrom(self.lastPoint, toPoint: self.lastPoint)
            }
            let newStroke = (self.currentLineColor, self.portraitScale * self.currentLineWidth, self.arrDetailStrokes)
            self.arrStrokes.append(newStroke)
            self.arrDetailStrokes = []
            UIGraphicsBeginImageContext(self.photoImgView.frame.size)
            self.photoImgView.image?.drawInRect(CGRect(x: 0, y: 0, width: self.photoImgView.frame.width, height: self.photoImgView.frame.height))
            self.tempImgView.image?.drawInRect(CGRect(x: 0, y: 0, width: self.photoImgView.frame.width, height: self.photoImgView.frame.height))
            self.photoImgView.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.tempImgView.image = nil
        }
    }
    override func touchesCancelled(touches:Set<UITouch>?, withEvent event:UIEvent?){
        self.tempImgView.image = nil
    }
    //MARK: - CollectionView
    
    func collectionView(collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        if self.currentList == 0 {
            return self.arrColors.count
        }else{
            return self.arrLineTypes.count
        }
    }
    func collectionView(collectionView:UICollectionView, cellForItemAtIndexPath indexPath:NSIndexPath) -> UICollectionViewCell {
        if self.currentList == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("colorCell", forIndexPath: indexPath)
            cell.backgroundColor = self.arrColors[indexPath.item]
            return cell
        }else{
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("lineCell", forIndexPath: indexPath) as! LineCollectionViewCell
            cell.lineNameLbl.text = self.arrLineTypes[indexPath.item]
            return cell
        }
    }
    func collectionView(collectionView:UICollectionView, didSelectItemAtIndexPath indexPath:NSIndexPath){
        if self.currentList == 0 {
            self.currentLineColor = indexPath.item
        }else{
            self.currentLineWidth = CGFloat((indexPath.item + 1) * 2)
        }
        self.hideColorPallete()
    }
    func collectionView(collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
        if self.currentList == 0 {
            return CGSize(width: 20, height: 20)
        }else{
            return CGSize(width: 100, height: 20)
        }
    }
}