//
//  PhotoCollectionViewCell.swift
//  TKPhotos
//
//  Created by Victor Salazar on 19/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class PhotoCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var photoImgView:UIImageView!
}