//
//  StrokeDetail+CoreDataProperties.swift
//  TKPhotos
//
//  Created by victor salazar on 20/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension StrokeDetail{
    @NSManaged var start_point:String
    @NSManaged var end_point:String
    @NSManaged var stroke:Stroke
}