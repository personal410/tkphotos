//
//  Photo+CoreDataProperties.swift
//  TKPhotos
//
//  Created by victor salazar on 20/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Photo{
    @NSManaged var date_created:NSDate
    @NSManaged var original_image:NSData
    @NSManaged var width:NSNumber
    @NSManaged var height:NSNumber
    @NSManaged var final_image:NSData
    @NSManaged var strokes:NSOrderedSet?
}