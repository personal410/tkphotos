//
//  Photo.swift
//  TKPhotos
//
//  Created by Victor Salazar on 19/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import Foundation
import CoreData
import UIKit
class Photo:NSManagedObject{
    class func createNewPhoto(originalImg:UIImage, finalImg:UIImage, arrStrokes:Array<(colorIdx:Int, lineWidth:CGFloat, arrSubstrokes:Array<(starPoint:CGPoint, endPoint:CGPoint)>)>) -> Bool{
        let ctxt = AppDelegate.getMoc()
        let newPhoto = NSEntityDescription.insertNewObjectForEntityForName("Photo", inManagedObjectContext: ctxt) as! Photo
        newPhoto.original_image = UIImagePNGRepresentation(originalImg)!
        newPhoto.final_image = UIImagePNGRepresentation(finalImg)!
        newPhoto.date_created = NSDate()
        newPhoto.width = originalImg.size.width
        newPhoto.height = originalImg.size.height
        for stroke in arrStrokes {
            let newStroke = NSEntityDescription.insertNewObjectForEntityForName("Stroke", inManagedObjectContext: ctxt) as! Stroke
            newStroke.color_index = stroke.colorIdx
            newStroke.line_width = Double(stroke.lineWidth)
            newStroke.photo = newPhoto
            for strokeDetail in stroke.arrSubstrokes {
                let newStrokeDetail = NSEntityDescription.insertNewObjectForEntityForName("StrokeDetail", inManagedObjectContext: ctxt) as! StrokeDetail
                newStrokeDetail.start_point = StringFromCGPoint(strokeDetail.starPoint)
                newStrokeDetail.end_point = StringFromCGPoint(strokeDetail.endPoint)
                newStrokeDetail.stroke = newStroke
            }
        }
        do{
            try ctxt.save()
            return true
        }catch let error as NSError {
            print("error: \(error.localizedDescription)")
            return false
        }
    }
    class func getAllPhotos() -> Array<Photo> {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest(entityName: "Photo")
        fetchReq.sortDescriptors = [NSSortDescriptor(key: "date_created", ascending: true)]
        do{
            let arrPhotos = try ctxt.executeFetchRequest(fetchReq) as! Array<Photo>
            return arrPhotos
        }catch let error as NSError {
            print(error.localizedDescription)
            return []
        }
    }
    func getSize() -> CGSize {
        return CGSize(width: self.width.doubleValue, height: self.height.doubleValue)
    }
}