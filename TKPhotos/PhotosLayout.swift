//
//  PhotosLayout.swift
//  TKPhotos
//
//  Created by Victor Salazar on 19/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
let MinColumnWidth:CGFloat = 100
class PhotosLayout:UICollectionViewLayout{
    var columnWidth:CGFloat = 0
    var numberColumns = 0
    var delegate:PhotosLayoutDelegate!
    var contentHeight:CGFloat = 0
    var cache:Array<UICollectionViewLayoutAttributes> = []
    override func prepareLayout(){
        cache = []
        contentHeight = 0
        let totalWidth = self.collectionView!.superview!.frame.width
        self.numberColumns = Int(floor(totalWidth / MinColumnWidth))
        self.columnWidth = totalWidth / CGFloat(self.numberColumns)
        var yOffset = [CGFloat](count: self.numberColumns, repeatedValue: 0)
        var minColumn = 0
        let numberItems = self.collectionView!.numberOfItemsInSection(0)
        for i in 0 ..< numberItems {
            let indexPath = NSIndexPath(forItem: i, inSection: 0)
            let itemSize = self.delegate.collectionView(self.collectionView!, sizeForPhotoAtIndexPath: indexPath)
            let newHeight:CGFloat = columnWidth * itemSize.height / itemSize.width
            let frame = CGRect(x: CGFloat(minColumn) * columnWidth, y: yOffset[minColumn], width: columnWidth, height: newHeight)
            let attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
            attributes.frame = frame
            cache.append(attributes)
            yOffset[minColumn] += newHeight
            let minYOffset = yOffset.minElement()!
            minColumn = yOffset.indexOf(minYOffset)!
            contentHeight = max(contentHeight, CGRectGetMaxY(frame))
        }
    }
    override func collectionViewContentSize() -> CGSize {
        return CGSize(width: self.collectionView!.frame.width, height: contentHeight)
    }
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes:Array<UICollectionViewLayoutAttributes> = []
        for attributes in self.cache {
            if CGRectIntersectsRect(attributes.frame, rect) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
}
protocol PhotosLayoutDelegate{
    func collectionView(collectionView:UICollectionView, sizeForPhotoAtIndexPath indexPath:NSIndexPath) -> CGSize
}