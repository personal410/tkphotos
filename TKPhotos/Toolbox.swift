//
//  Toolbox.swift
//  TKPhotos
//
//  Created by victor salazar on 22/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class Toolbox{
    class func showAlertWithTitle(title:String, withMessage message:String, withOkHandler handler:((alertAction:UIAlertAction) -> Void)? = nil, inViewCont viewCont:UIViewController){
        let alertCont = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertCont.addAction(UIAlertAction(title: "OK", style: .Default, handler: handler))
        viewCont.presentViewController(alertCont, animated: true, completion: nil)
    }
    class func isLandscape() -> Bool {
        let screen = UIScreen.mainScreen()
        return screen.bounds.size.width > screen.bounds.size.height
    }
    class func arrAvailableColors() -> Array<UIColor> {
        return [UIColor.blackColor(), UIColor.blueColor(), UIColor.redColor(), UIColor.yellowColor()]
    }
}
public func * (point:CGPoint, scalar:CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}
public func / (point:CGPoint, scalar:CGFloat) -> CGPoint {
    return CGPoint(x: point.x / scalar, y: point.y / scalar)
}
public func StringFromCGPoint(point:CGPoint) -> String {
    return String(format: "{%0.2f,%0.2f}", point.x, point.y)
}