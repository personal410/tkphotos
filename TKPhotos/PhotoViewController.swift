//
//  PhotoViewController.swift
//  TKPhotos
//
//  Created by victor salazar on 21/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
let AnimationTime = 0.05
class PhotoViewController:UIViewController{
    //MARK: - Variables
    let arrColors = Toolbox.arrAvailableColors()
    var image:UIImage!
    var photo:Photo!
    var photoImgView = UIImageView()
    var isLandscape:Bool = false
    var actualScale:CGFloat = 0
    var portraitScale:CGFloat = 0
    var landscapeScale:CGFloat = 0
    var factorLineWidth:CGFloat = 0
    var totalFrames = 0
    var currentFrames = 0
    var arrStrokes:Array<Stroke> = []
    var currentStroke = 0
    var currentDetailStroke = 0
    var isPlaying = false
    var sliderSelected = false
    //MARK: - IBOutlet
    @IBOutlet weak var heightToolbar:NSLayoutConstraint!
    @IBOutlet weak var slider:UISlider!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.image = UIImage(data: self.photo.original_image)!
        self.photoImgView.image = UIImage(data: self.photo.final_image)
        self.view.addSubview(self.photoImgView)
        self.slider.minimumValue = 0
        if let arrStrokesTemp = self.photo.strokes {
            self.arrStrokes = arrStrokesTemp.array as! Array<Stroke>
            totalFrames = self.arrStrokes.map(){$0.stroke_details.array.count}.reduce(0, combine: +)
            self.slider.maximumValue = Float(self.totalFrames)
            self.slider.value = self.slider.maximumValue
        }
        var avaibleWidth = self.view.frame.width
        var availableHeight = self.view.frame.height
        if avaibleWidth > availableHeight {
            self.isLandscape = true
            let heightTemp = availableHeight
            availableHeight = avaibleWidth
            avaibleWidth = heightTemp
        }
        self.heightToolbar.constant = self.isLandscape ? 32 : 44
        let avaiblePortraitHeight = availableHeight - 108
        let scaleWidthPor = self.image.size.width / avaibleWidth
        let scaleHeightPor = self.image.size.height / avaiblePortraitHeight
        self.portraitScale = max(scaleWidthPor, scaleHeightPor)
        let avaibleLandscapeHeight = avaibleWidth - 64
        let scaleWidthLand = self.image!.size.width / availableHeight
        let scaleHeightLand = self.image!.size.height / avaibleLandscapeHeight
        self.landscapeScale = max(scaleWidthLand, scaleHeightLand)
        self.factorLineWidth = self.portraitScale / self.landscapeScale
        self.photoImgView.frame = self.calculatePhotoFrame(self.view.frame.width, height: (isLandscape ? avaibleLandscapeHeight : avaiblePortraitHeight))
    }
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        UIView.animateWithDuration(0.25, animations:{
            let aditional:CGFloat = UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 64 : 32
            let diff:CGFloat = UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 64 : 104
            self.photoImgView.frame = self.calculatePhotoFrame(self.view.frame.height + aditional, height: self.view.frame.width - diff)
            self.heightToolbar.constant = UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 32 : 44
        })
        self.isPlaying = false
    }
    //MARK: - IBAction
    @IBAction func play(){
        if self.isPlaying {
            self.isPlaying = false
        }else{
            if self.currentStroke == 0 && self.currentDetailStroke == 0 {
                self.photoImgView.image = self.image
                self.slider.value = 0
            }
            self.performSelector(#selector(PhotoViewController.animate), withObject: nil, afterDelay: AnimationTime)
            self.isPlaying = true
        }
    }
    @IBAction func stop(){
        self.currentStroke = 0
        self.currentDetailStroke = 0
        self.currentFrames = 0
        self.isPlaying = false
        self.slider.value = Float(self.totalFrames)
        self.photoImgView.image = UIImage(data: self.photo.final_image)
    }
    @IBAction func sliderDidChangeValue(){
        self.sliderSelected = false
        self.slider.setValue(floor(self.slider.value), animated: true)
        var currentValue = Int(self.slider.value)
        self.currentStroke = 0
        self.currentDetailStroke = 0
        self.currentFrames = 0
        for stroke in self.arrStrokes {
            let strokeDetails = stroke.stroke_details.array as! Array<StrokeDetail>
            if currentValue < strokeDetails.count {
                self.currentDetailStroke = currentValue
                self.currentFrames += currentValue
                break
            }else{
                currentValue -= strokeDetails.count
                self.currentFrames += strokeDetails.count
                self.currentStroke += 1
            }
        }
        self.photoImgView.image = self.image
        for i in 0 ..< self.currentStroke {
            let stroke = self.arrStrokes[i]
            let strokeDetails = stroke.stroke_details.array
            for j in 0 ..< strokeDetails.count {
                self.addLineToPhoto(i, strokeDetailIdx: j)
            }
        }
        for j in 0 ..< self.currentDetailStroke {
            self.addLineToPhoto(self.currentStroke, strokeDetailIdx: j)
        }
        self.performSelector(#selector(PhotoViewController.animate), withObject: nil, afterDelay: AnimationTime)
    }
    @IBAction func sliderDidSelect(){
        self.sliderSelected = true
    }
    @IBAction func exportImage(){
        UIImageWriteToSavedPhotosAlbum(UIImage(data: self.photo.final_image)!, self, #selector(PhotoViewController.didExportImage(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    //MARK: - Auxiliar
    func calculatePhotoFrame(width:CGFloat, height:CGFloat) -> CGRect {
        if width > height {
            self.isLandscape = true
            self.actualScale = self.landscapeScale
        }else{
            self.isLandscape = false
            self.actualScale = self.portraitScale
        }
        let imageWidth = floor(self.image!.size.width / self.actualScale)
        let imageHeight = floor(self.image!.size.height / self.actualScale)
        let y:CGFloat = (height - imageHeight) / 2
        let x:CGFloat = (width - imageWidth) / 2
        return CGRect(x: x, y: y, width: imageWidth, height: imageHeight)
    }
    func animate(){
        if self.isPlaying && !self.sliderSelected {
            self.addLineToPhoto(self.currentStroke, strokeDetailIdx: self.currentDetailStroke)
            self.currentFrames += 1
            self.slider.value += 1
            if self.currentFrames == self.totalFrames {
                self.currentStroke = 0
                self.currentDetailStroke = 0
                self.currentFrames = 0
                self.isPlaying = false
            }else{
                self.currentDetailStroke += 1
                let stroke = self.arrStrokes[self.currentStroke]
                let strokeDetails = stroke.stroke_details.array as! Array<StrokeDetail>
                if currentDetailStroke == strokeDetails.count {
                    self.currentStroke += 1
                    self.currentDetailStroke = 0
                }
                self.performSelector(#selector(PhotoViewController.animate), withObject: nil, afterDelay: AnimationTime)
            }
        }
    }
    func addLineToPhoto(strokeIdx:Int, strokeDetailIdx:Int){
        UIGraphicsBeginImageContext(self.photoImgView.frame.size)
        let context = UIGraphicsGetCurrentContext()
        self.photoImgView.image?.drawInRect(CGRect(x: 0, y: 0, width: self.photoImgView.frame.size.width, height: self.photoImgView.frame.size.height))
        let stroke = self.arrStrokes[strokeIdx]
        let strokeDetails = stroke.stroke_details.array as! Array<StrokeDetail>
        let strokeDetail = strokeDetails[strokeDetailIdx]
        let fromPoint = CGPointFromString(strokeDetail.start_point) / self.actualScale
        let toPoint = CGPointFromString(strokeDetail.end_point) / self.actualScale
        CGContextMoveToPoint(context, fromPoint.x, fromPoint.y)
        CGContextAddLineToPoint(context, toPoint.x, toPoint.y)
        CGContextSetLineCap(context, CGLineCap.Round)
        let factor = self.isLandscape ? self.factorLineWidth : 1
        CGContextSetLineWidth(context, CGFloat(stroke.line_width.doubleValue) / self.portraitScale * factor)
        CGContextSetStrokeColorWithColor(context, self.arrColors[stroke.color_index.integerValue].CGColor)
        CGContextStrokePath(context)
        self.photoImgView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    func didExportImage(image:UIImage!, didFinishSavingWithError error:NSError!, contextInfo:AnyObject!){
        Toolbox.showAlertWithTitle("Imagen", withMessage: "La imagen se exporto correctamente", inViewCont: self)
    }
}