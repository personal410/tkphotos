//
//  CustomNavViewController.swift
//  TKPhotos
//
//  Created by victor salazar on 20/04/16.
//  Copyright © 2016 Victor Salazar. All rights reserved.
//
import UIKit
class CustomNavViewController:UINavigationController, UINavigationControllerDelegate{
    let expantionTransitionDelegate = ExpantionTransitionDelegate()
    override func viewDidLoad(){
        super.viewDidLoad()
        self.delegate = self
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.All
    }
    func navigationController(navigationController:UINavigationController, animationControllerForOperation operation:UINavigationControllerOperation, fromViewController fromVC:UIViewController, toViewController toVC:UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if (toVC is CreateImageViewController) || (fromVC is CreateImageViewController) {
            return nil
        }else{
            expantionTransitionDelegate.isPresenting = operation == .Push
            return expantionTransitionDelegate
        }
    }
}